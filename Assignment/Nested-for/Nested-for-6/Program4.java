/*Problem 4:
Write a program to take a range as input from the user and print perfect cubes between that range.
Input: Enter start: 1
Enter end: 100
Output: perfect cubes between 1 and 100
*/

import java.io.*;
class Program4{

	void cube(int start, int end){
	
		int cubeOfNumber = 0;
		System.out.println("Perfect cubes between 1 and 100: ");
		for(int i = start; i<=end;i++){
			cubeOfNumber = i*i*i;

			if(cubeOfNumber <= 100){
				System.out.print(cubeOfNumber+ " ");
			}
		}

	}
	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start: ");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter end: ");
                int end = Integer.parseInt(br.readLine());

		Program4  num = new Program4();
		num.cube(start,end);
	}
}

