/*Write a program in which students should enter marks of 5 different subjects. If all subject
having above passing marks add them and provide to switch case to print grades(first class
second class), if student get fail in any subject program should print “You failed in exam”
*/
import java.io.*;
class Program1 {
	public static void main(String [] args)throws IOException {
		System.out.println("Enter Your Marks");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Your Maths Marks");
		float marks1 = Float.parseFloat(br.readLine());

		System.out.println("Enter Your Physics Marks");
		float marks2 = Float.parseFloat(br.readLine());

		System.out.println("Enter Your Chemistry Marks");
                float marks3 = Float.parseFloat(br.readLine());

		System.out.println("Enter Your History Marks");
                float marks4 = Float.parseFloat(br.readLine());

		System.out.println("Enter Your Science Marks");
                float marks5 = Float.parseFloat(br.readLine());
		
		float sum;
		float avg;
		float min = 35.0f;
		if(marks1>=min && marks2>=min && marks3>=min && marks4>=min && marks5>=min) {
			sum = marks1+marks2+marks3+marks4+marks5;	
			avg = (sum/500)*100;

			char ch;
			if(avg>75) {
				ch = 'A';
			} else if(avg>60) {
				ch = 'B';
			} else if(avg>50) {
				ch = 'C';
			} else if(avg>40) {
				ch = 'D';
			} else if(avg>35) {
				ch = 'E';
			} else {
				ch = 'F';
			}
			switch (ch){
				case 'A':
					System.out.println("You passed with First class");
					break;
				case 'B':
					System.out.println("You passed with Second class");
					break;
				case 'C':
					System.out.println("You passed with Third class");
					break;
				case 'D':
					System.out.println("You barely manage to pass");
					break;
				case 'E':
					System.out.println("You passed");
					break;
				case 'F':
					System.out.println("You failed");
					break;
			}
		} else {
			System.out.println("You Failed");
		}
	}
}
