interface Demo6 {

	static void fun() {

		System.out.println("Demo fun");
	}

}

class Child6 implements Demo6 {

	void fun() {

		System.out.println("Child fun");
		Demo6.fun();
	}
}

class Client6 {

	public static void main(String [] args) {

		Child6 obj = new Child6();
		obj.fun();
	}
}
