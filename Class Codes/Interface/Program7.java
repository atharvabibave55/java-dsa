interface Demo7 {

	int x = 10;
	void fun();

}

class Demo7Child implements Demo7 {

	public void fun() {

		System.out.println(x);

	}

}

class Client7 {

	public static void main(String [] args) {
		Demo7 obj = new Demo7Child();
		obj.fun();
	}

}
