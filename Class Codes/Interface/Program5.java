interface Demo5 {

	void fun();

	default void gun() {

		System.out.println("gun demo");
	}
}

class Child5 implements Demo5 {

	public void gun() {

		System.out.println("Child gun");
	}

	public void fun() {

		System.out.println("Child fun");

	}
}

class Client5 {

	public static void main(String [] args) {

		Demo5 obj = new Child5();
		obj.gun();
		obj.fun();
	}
}
