interface Demo1 {

	void fun();
	void gun();

}

abstract class Child1 implements Demo1 {

	public void gun() {

		System.out.println("In gun");

	}
}

class Child2 extends Child1 {


	public void fun() {

		System.out.println("In fun");
	}
}

class Client1 {


	public static void main(String [] args) {

		Demo1 obj = new Child2();
		obj.fun();
		obj.gun();
	}
}
