interface Demo4 {

	void fun();

	default void gun() {

		System.out.println("In gun demo");

	}
}

class DemoChild implements Demo4 {


	public void fun() {

		System.out.println("In fun Demo Child");

	}
}

class Client {

	public static void main(String [] args) {

		DemoChild obj = new DemoChild();
		obj.fun();
		obj.fun();
	}
}
