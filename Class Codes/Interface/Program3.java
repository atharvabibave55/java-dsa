interface Demo2 {

	void fun();
}
interface Demo3 {

	void fun();
}

class Child3 implements Demo2, Demo3 {

	public void fun() {

		System.out.println("In child fun");
	}
}

class Client2 {

	public static void main(String [] args) {

		Demo2 obj = new Child3();
		obj.fun();
	}
}
