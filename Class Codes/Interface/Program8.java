interface Demo8 {

	int x = 10;
}

interface Demo9 {

	int y = 20;
}

class Child8 implements Demo8, Demo9 {

	int x = 30;
	void fun() {

		System.out.println(x);
		System.out.println(Demo8.x);
		System.out.println(Demo9.y);
	}
}

class Client8 {

	public static void main(String [] args) {

		Child8 obj = new Child8();
		obj.fun();
	}
}
