class Parent2 {

	int x = 10;
	static int y = 20;

	static {

		System.out.println("Parent static block");

	}
	Parent2() {

		System.out.println("Parent static block");

	}
	void methodOne() {
		
		System.out.println(x);
		System.out.println(y);
	}
	static void methodTwo() {

		System.out.println(y);

	}
}

class Child1 extends Parent2 {

	static {

		System.out.println("In child static block");

	}

	Child1() {

		System.out.println("In child constructor");
	}
}

class Client2 {

	public static void main(String [] args) {
		Child1 obj = new Child1();
		obj.methodOne();
		obj.methodTwo();
	}
}

