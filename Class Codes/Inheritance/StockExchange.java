class NSE {

	NSE() {
	
		System.out.println("NSE means National Stock Exchnage in India");

	}

}

class BSE extends NSE {

	BSE() {
		
		System.out.println("BSE means Bombay Stock Exchange");
	}

	BSE(int x) {
		
		this();
		System.out.println("Market up time 9:00 am to 3:30pm");

	}

}

class Trader {

	public static void main(String [] args) {

		NSE stock = new BSE(9);

	}
}
