class Demo1 {

	int x = 10;
	int y = 20;

	void access() {

		System.out.println(x);
		System.out.println(y);
	}
}

class Child4 extends Demo1 {

	int x = 100;
	int y = 200;

	void access() {

		super.access();
		System.out.println(this.x);
		System.out.println(this.y);

	}
}

class Client4 {

	public static void main(String [] args) {

		Child4 obj = new Child4();
		obj.access();
	}

}
