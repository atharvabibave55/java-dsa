
class Hotel {

	Hotel() {
		
		System.out.println("In parent constructor");

	}
}

class DiningHall extends Hotel {

		DiningHall() {
			
			System.out.println("In child constructor Dining Hall");

		}
}

class SwimmingPool extends Hotel {

	SwimmingPool() {

		System.out.println("In child constructor SwmmingPool");

	}
}

class Main {

	public static void main(String [] args) {

		DiningHall obj1 = new DiningHall();
		SwimmingPool obj2 = new SwimmingPool();
	}
}
