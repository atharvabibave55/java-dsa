class Parent3 {

	int x = 10;
	static int y = 20;

	Parent3() {

		System.out.println("Parent");

	}

}

class Child3 extends Parent3 {

	int x = 100;
	static int y = 200;

	Child3() {

		System.out.println("Child");

	}
	void access1() {
	
		System.out.println("super:"+super.x);
		System.out.println("super:"+super.y);
		System.out.println("this:"+x);
		System.out.println("this:"+y);
	}
}

class Client3 {

	public static void main(String [] args) {

		Child3 obj = new Child3();
		obj.access1();
	}
}
