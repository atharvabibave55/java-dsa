/*
method: public synchronized String substring(int beginIndex, int endIndex);

description:
	Creates a substring of this StringBuffer, starting at a specific index and ending at one characterbefore a specific index

parameter:
	Integer(beginIndex index to start at(inclusive,base 0)).
	Integer(endIndex index to end at(exclusie)).
	String(str the new<code>String</code>to insert).

return type: String(new String which is a substring of this StringBuffer).
*/

class SubstringDemo{
	public static void main(String [] args){

		StringBuffer str1 = new StringBuffer("know the code till the core");
		String str2 = str1.substring(14,27);
		System.out.println(str2);
	}
}
