/*
method: public synchronized int length();

description:
	Get the length of the <code>String</code>this<code>StringBuffer</code>would create.
	Not to be confused with <em>capacity</em> of the<code>StringBuffer</code>

parameter: No parameter

return type: Integer(the length of this <code> StringBuffer</code>
*/

class LengthDemo{
	public static void main(String [] args){
	       StringBuffer str1 = new StringBuffer("Core2Web");
       System.out.println(str1.length());
	}
}	
