/*
method: public synchronized int indexOf(String str , int fromIndex);

description:
	Finds the first instance of a String in this StringBuffer, starting at a given index.
	If the starting index is less than 0, search start at the begining of this String.
	If the starting index is greater than the length of this String, or the substring is not found -1 is returned.

parameter: String(str String to find).
	   Integer(fromIndex index to start the search).

return type: Integer(location(base0)of String, or -1 if not found).
*/

class IndexOfDemo{
	public static void main(String [] args){
		StringBuffer str1 = new StringBuffer("Core2Web");
		System.out.println(str1.indexOf("e"));
	}
}
