/*
method:  pubic synchronized int lastIndexOf(String, int fromIndex);

description:
	Finds the last instance of a String in this StringBuffer, starting at a given index.
	If starting index is greater tha maximum valid index, then the search begins at the end off this String.
	If starting index is less than zero or the substring is not found, -1 is returned.

parameter: String(str String to find),
	Integer(fromIndex to start the search).

return type: integer(location(base0) of the string, or -1 if not found.
*/

class LastIndexOfDemo{
	public static void main(String [] args){
		StringBuffer str1 = new StringBuffer("Core2Web");
		System.out.println(str1.lastIndexOf("e"));
	}
}
