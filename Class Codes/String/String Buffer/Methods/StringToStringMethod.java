/*
method: public String toString();

description:
	convert this <code>StringBuffer</code> to a<code>String</code>.
	The string is composed of the characters currently in this StringBuffer.
	Note that the result is a copy, and that future mdifications to this buffer do not affect the string.

parameter: no Parameter

return type: String(the character in this StringBuffer).
*/

class ToStringDemo{

	public static void main(String [] args){
		StringBuffer str1 = new StringBuffer("Know the code till core");
		String str2 = "Core2Web:";
		String str3 = str1.toString();
		String str4 = str2.concat(str3);
		System.out.println(str4);
	}
}

