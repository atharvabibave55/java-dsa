/*
method: public synchronised StringBuffer replace(int start , int end , String str);

description: 
	Replace characters between index <code>start</code> (inclusive) and <code>end</code>
	If <code>is larger than the size of this StringBuffer, all characters after <code> start </code> are replaced.

parameters:
	Integer(start the beginning index of characters to delete(inclusive)),
	Integer(end the ending index of character to delete(exclusive)),
	String(str the new<code>String</code>to insert).

return type: StringBuffer(this<code>StringBuffer</code>).
*/

class ReplaceDemo{
	public static void main(String [] args){
		StringBuffer str1 = new StringBuffer("Know the code till the core");
		System.out.println(str1);
	}
}

