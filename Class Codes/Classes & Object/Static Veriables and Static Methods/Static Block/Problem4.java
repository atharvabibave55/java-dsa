class Demo3 {
	
	static int x = 10;

	static {
	
		static int x = 20;// illigal start of expression
	}

	void fun() {
		
		static int y = 30;// illigal start of expression

	}

	static void gun() {
		
		static int z = 40;// illigal start of expression

	}
}
