class Demo1 {
	int x = 10;
	static int y = 20;

	static {
		System.out.println("Static block 1");
	}
	public static void main(String [] args) {
		System.out.println("Main Method");

		Demo1 obj = new Demo1();
		System.out.println(obj.x);
	}
	static {
		System.out.println("Static block 2");
		System.out.println(y);
	}
}
