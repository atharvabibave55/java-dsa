class Demo2 {
	
	static {
		
		System.out.println("Static block 1");
	}

	public static void main(String [] args) {

		System.out.println("In Demo2 main");

	}
}

class Client {
	
	static {

		System.out.println("In static block 2");

	}

	public static void main(String [] args) {
		
		System.out.println("In client main");

	}

	static {
		
		System.out.println("Static block 3");

	}
}
