class Demo {
	
	int x = 10;
	Demo() {
		
		System.out.println("In constructor");
		System.out.println(x);

	}

	void fun() {

		System.out.println(x);

	}

	public static void main(String [] args) {
		
		Demo obj1 = new Demo();
		Demo obj2 = new Demo();

	}
}
