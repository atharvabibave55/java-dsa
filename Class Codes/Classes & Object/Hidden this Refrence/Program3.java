class Demo2 {
	
	int x = 10;

	Demo2() {
	
		System.out.println("In no-arg constructor");

	}
	Demo2(int x) {
		
		this();
		System.out.println("In para constructor");

	}
	public static void main(String [] args) {
		
		Demo2 obj = new Demo2(50);
	}

}
