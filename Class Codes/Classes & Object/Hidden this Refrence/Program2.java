class Demo1 {
	
	int x = 10;

	Demo1() {	// Internally--> Demo( Demo this )
		
		System.out.println("In no-arg Constructor");
		
	}
	Demo1(int x) {	// Internally--> Demo( Demo this , int x )
		
		System.out.println("In para constructor");

	}
	public static void main(String [] args) {
		
		Demo1 obj1 = new Demo1();	// Internally--> Demo( obj1 )
		Demo1 obj2 = new Demo1(10);	// Internally--> Demo( obj1 , 10 )

	}
}
