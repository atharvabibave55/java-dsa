class Player {
	
	private int jerNo = 0;
	private String name = null;// without initialization chalat nahi mhanun default value dili
	
	Player(int jerNo,String name) {		//Internally:--> Player(Player this, int jerNo, String name)
	
		//this refrence fakta instance chya goshtin sathi asto
		//setter method mhanun work karel
		this.jerNo = jerNo;
	// Explanation:--> this.jerNo kel karan aplyala instance veriable madhe change karycha hota mhanun jerNo = jerNo kel asta tr to swata ch swata la assign krt asta ani output 0 ani null ch print zala asta.
		
		this.name = name;
		System.out.println("In player constructor");

	}
	void info() {
	
		//getter method mhanun work karel
		System.out.println(jerNo + " = " + name);
		System.out.println("In method info");

	}
}

class Client {

	public static void main(String [] args) {
		
		Player obj1 = new Player(18,"Virat");	//Player(obj1,18,Virat)
		obj1.info();	// info(obj1)

		Player obj2 = new Player(7,"MSD");	//Player(obj2,7,MSD)
		obj2.info();	//info(obj2)

		Player obj3 = new Player(8,"Jaddu");	//Player(obj3,8,Jaddu)
		obj3.info();	//info(obj3)

	}
}
