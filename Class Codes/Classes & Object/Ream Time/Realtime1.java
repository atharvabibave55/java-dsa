class LoginCredentials{

	static String emailId = "atharvabibave55@gmail.com";
	String name = "Atharv Bibave";
	int age = 21;

	void info(){
		System.out.println("Your email id: "+emailId);
		System.out.println("Your name: "+name);
		System.out.println("Your age: "+age);

	}
}
class ChangingCredentials{
	public static  void main(String [] args){

		LoginCredentials loginId1 = new LoginCredentials();
		LoginCredentials loginId2 = new LoginCredentials();
		
		System.out.println("First object call");
		loginId1.info();
		
		System.out.println("Second object call");
		loginId2.info();
		
		
		System.out.println("-----------------");

		loginId2.emailId = "changedemailid77@gmail.com";
		
		System.out.println("First object call after Change");
		loginId1.info();
		
		System.out.println("Second object call after Change");
                loginId2.info();
		
	}
}


