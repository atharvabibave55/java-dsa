class Demo1{
	int x = 10;
	private int y = 20;
	static int z = 50;

	void display(){
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
	}
}
class Client{
	public static void main(String [] args){
		Demo1 obj1 = new Demo1();
		Demo1 obj2 = new Demo1();

		obj1.display();

		obj1.x = 100;
		obj2.z = 300;

		obj1.display();
		obj2.display();
	}
}

