import java.util.Scanner;
import java.io.*;

class DataUnderflowException extends IOException {
	
	DataUnderflowException(String msg) {
		
		super(msg);

	}
}

class Client2 {

	public static void main(String [] args) throws DataUnderflowException {
		Scanner sc = new Scanner(System.in);
		int arr[] = new int[5];
		System.out.println("Enter positive elements");

		for(int i = 0; i<arr.length; i++) {
			int data = sc.nextInt();
			if(data<0) {
				throw new DataUnderflowException("Value must be positive");
			}
			arr[i] = data;
		}
	}
}
