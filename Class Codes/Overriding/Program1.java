class Parent1 {

	Parent1() {

		System.out.println("Parent Constructor");
	}

	void property() {

		System.out.println("Home, car, gold");

	}
	void marry() {

		System.out.println("Deepika Padukone");
	}

}

class Child1 extends Parent1 {

	Child1() {

		System.out.println("Child Constructor");
	}

	void marry() {

		System.out.println("Disha Patani");
	}

}

class Client1 {

	public static void main(String [] args) {

		Child1 obj = new Child1();
		obj.property();
		obj.marry();
	}

}	
