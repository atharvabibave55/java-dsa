class IPL {

	void matchInfo(String team1, String team2) {

		System.out.println(team1 + " Vs " + team2);
	}

	void matchInfo(String team1, String team2, String Venue) {

		System.out.println(team1 + " Vs " + team2);
		System.out.println("Venue: " + Venue); 
	}
}	

class Client4 {

	public static void main(String [] args) {

		IPL ipl2023 = new IPL();
		ipl2023.matchInfo("GT","CSK");
		ipl2023.matchInfo("GT","CSK","Ahmedabad");
	}
}
