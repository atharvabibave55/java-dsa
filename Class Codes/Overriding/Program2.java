class Parent2 {

	Parent2() {

		System.out.println("Parent Constructor");

	}

	void fun() {

		System.out.println("In parent fun");

	}

}

class Child2 extends Parent2 {

	Child2() {

		System.out.println("Child Constructor");
	}

	void fun() {

		System.out.println("In fun");
	}
}

class Client2 {

	public static void main(String [] args) {

		Parent2 obj1 = new Child2();
		obj1.fun();
	}
}
