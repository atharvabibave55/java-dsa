class Match {

	void matchType() {

		System.out.println("T20/ODI/TEST");

	}
}

class IPLMatch extends Match {

	void matchType() {
	
		System.out.println("IPL");
	}

}

class TestMatch extends Match {

	void matchType() {

		System.out.println("Test");
	}
}

class Client5 {

	public static void main(String [] args) {
		
		Match type1 = new IPLMatch();
		type1.matchType();

		Match type2 = new TestMatch();
		type2.matchType();
	}
}
