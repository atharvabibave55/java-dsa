class Parent3 {

	Parent3() {

		System.out.println("Parent3 constructor");

	}
	void fun() {

		System.out.println("In parent fun");
	}

}

class Child3 extends Parent3 {

	Child3() {

		System.out.println("In child constructor");

	}

	void fun(int x) {

		System.out.println("In child fun");

	}

}

class Client3 {

	public static void main(String [] args) {

		Child3 obj = new Child3();
		obj.fun(1);
	}
}
