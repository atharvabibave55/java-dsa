class Outer3 {

	void m1() {

		System.out.println("Outer m1");

	}	

	static class Inner3 {

		void m1() {

			System.out.println("Inner m1");
		}
	}
}

class Client4 {

	public static void main(String [] args) {

		Outer3.Inner3 obj = new Outer3.Inner3();
		obj.m1();
	}
}

