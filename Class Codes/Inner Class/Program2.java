class Outer1 {

	class Inner1 {

		void m1() {

			System.out.println("Inner m1");
		}
	}

	void m2() {

		System.out.println("Outer m2");
	}

	public static void main(String [] args) {

		Inner1 obj = new Outer1().new Inner1();
		obj.m1();
	}
}
