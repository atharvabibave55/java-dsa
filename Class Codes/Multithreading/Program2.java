class MyThread2 extends Thread {

	public void run() {

		System.out.println("Child Thread= "+ Thread.currentThread().getName());

		for(int i = 0; i<10; i++) {

			System.out.println("In run");

			try {
				Thread.sleep(1000);
			} catch(InterruptedException ie) {

				ie.printStackTrace();
				System.out.println("Hello Exception");	
			}
		}
	}
}

class ThreadDemo2 {

	public static void main(String [] args) {

		System.out.println("Main Thread = "+ Thread.currentThread().getName());

		MyThread2 obj = new MyThread2();
		obj.start();

		for(int i = 0; i<10; i++) {

			System.out.println("In main");
			Thread.sleep(1000);
		}
	}
}
