class Demo1 {

	int fun(int x) {

		return x;
	}

	float fun(int y){

		return y;
	}
}

class Client1 {

	public static void main(String [] args) {
	
		Demo1 obj = new Demo1();
		int ret = obj.fun(10);

	}
}
// Error fun() already defined !
