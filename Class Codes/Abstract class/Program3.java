abstract class Army {

	void rules() {

		System.out.println("Army rules");

	}
	abstract void trainning();
}

class Regiment extends Army {

	void trainning() {

		System.out.println("Trainning");
	}
}

class Client3 {

	public static void main(String [] args) {

		Army obj = new Regiment();
		obj.rules();
		obj.trainning();

	}
}
