abstract class Bakery {

	void cakeFlavours() {

		System.out.println("Cakes");
	}
	abstract void price();

}

class CakeShop extends Bakery {

	void price() {

		System.out.println("Price");

	}
}

class Client4 {

	public static void main(String []args){
		Bakery cake = new CakeShop();
		cake.cakeFlavours();
		cake.price();
	}
}
