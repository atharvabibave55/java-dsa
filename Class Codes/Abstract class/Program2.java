abstract class Company {

	void plant() {

		System.out.println("Company plant");
	}
	abstract void management();

}

class Manager extends Company {

	void management() {

		System.out.println("Managing");
	}
}

class Client2 {

	public static void main(String [] args) {

		Company obj = new Manager();
		obj.plant();
		obj.management();
	}
}
